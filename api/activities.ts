import httpClient from '@capytale/activity.js/backend/capytale/http'
import type { ActivityBase, ActivityMetadata, ActivityDetails } from '~/types/activities'

// Definit le endpoint de l'API
const myActivitiesApiEp = '/web/c-hdls/api/my-activities'

function metaDataEp(nid: number | string) {
  return `/web/c-hdls/api/metadata/${nid}`
}

export function apiFetchActivities(): Promise<ActivityBase[]> {
  // @ts-expect-error - typage getJsonAsync
  return httpClient.getJsonAsync<ActivityBase[]>('/web/c-hdls/api/all-activities')
}

export function apiGetMetadata(nid: number | string) {
  return httpClient.getJsonAsync<ActivityMetadata>(metaDataEp(nid))
}

export function apiPutMetadata(nid: number | string, data: ActivityMetadata) {
  return httpClient.putJsonAsync(metaDataEp(nid), data)
}

export function apiLoadActivityDetails(nid: number | string) {
  return httpClient.getJsonAsync<ActivityDetails>(`/web/c-hdls/api/all-details/${nid}`)
}

export function apiGetWholeDetails(nid: number | string) {
  return httpClient.getJsonAsync<ActivityDetails>(`/web/c-hdls/api/refresh-details/${nid}`)
}

export async function apiDeleteActivity(nids: number[]) {
  return httpClient.postGetJsonAsync<{ deleted: number[], notDeleted: number[] }>(
    myActivitiesApiEp,
    { action: 'delete', nids },
  )
}

export async function apiCloneActivity(nid: number | string, title?: string) {
  return httpClient.postGetJsonAsync<any>(
    myActivitiesApiEp,
    { action: 'clone', nid, title },
  )
}

export async function apiLockMode(nid: number | string, mode: 'lock' | 'unlock') {
  return httpClient.postGetJsonAsync<any>(
    myActivitiesApiEp,
    { action: `${mode}Mode`, nid },
  )
}

export async function apiMoveActivities(nids: number[], tid: number) {
  return httpClient.postGetJsonAsync<any>(
    myActivitiesApiEp,
    { action: 'moveFolder', nids, tid },
  )
}

export async function apiUntagActivity(nid: number, tid: number) {
  return httpClient.postGetJsonAsync<any>(
    myActivitiesApiEp,
    { action: 'untag', nid, tid },
  )
}

export async function apiBulkArchive(nid: number, corbeilleTid: number) {
  return httpClient.postJsonAsync(
    myActivitiesApiEp,
    { action: 'bulkArchive', nid, corbeilleTid },
  )
}

export async function apiReplaceTags(nids: number[], tids: number[]) {
  return httpClient.postJsonAsync(
    myActivitiesApiEp,
    { action: 'replaceTags', nids, tids },
  )
}
