import httpClient from '@capytale/activity.js/backend/capytale/http';
import type { BibActivityDetail } from "~/types/bib";

const bibEp = '/web/c-hdls/api/bibliotheque';


/**
 * Récupère les activités de la bibliothèque.
 */
function fetchActivities(): Promise<BibActivityDetail[]> {
  return httpClient.getJsonAsync<any>(bibEp);
}

/**
 * Récupère une activité de la bibliothèque.
 */
function fetchOneActivity(nid: number): Promise<BibActivityDetail> {
  return httpClient.getJsonAsync<any>(`${bibEp}/${nid}`);
}

/**
 * Récupère l'abstract complet d'une activité.
 * 
 * @param nid 
 * @returns 
 */
function fetchFullAbstract(nid: number): Promise<string> {
  return httpClient.getJsonAsync<any>(`${bibEp}/${nid}/abstract`);
}

export default {
  fetchActivities,
  fetchOneActivity,
  fetchFullAbstract,
}