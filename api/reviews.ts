import httpClient from '@capytale/activity.js/backend/capytale/http'
import type { Review, ComiteeReview } from '~/types/reviews'

function getReviews(nid: number): Promise<Review[]> {
  return httpClient.getJsonAsync<Review[]>(`/web/c-hdls/api/get_reviews/${nid}`) as Promise<Review[]>
}

function getComiteeReview(nid: number): Promise<ComiteeReview> {
  return httpClient.getJsonAsync<ComiteeReview>(`/web/c-hdls/api/get_comitee_review/${nid}`) as Promise<ComiteeReview>
}

function saveComiteeReview(nid: number, data: ComiteeReview): Promise<number> {
  return httpClient.putGetJsonAsync<number>(`/web/c-hdls/api/save_comitee_review/${nid}`, data) as Promise<number>
}

function askForMedal(nid: number): Promise<number> {
  return httpClient.putGetJsonAsync<number>(`/web/c-hdls/api/ask_for_medal/${nid}`) as Promise<number>
}

function addReview(data: { nid: number, content: string, criteria: { [key: string]: number } }): Promise<number> {
  return httpClient.putGetJsonAsync<number>(`/web/c-hdls/api/add_review`, data)
}

function editReview(rid: number, data: { content: string, criteria: { [key: string]: number } }): Promise<number> {
  return httpClient.patchGetJsonAsync<number>(`/web/c-hdls/api/review/${rid}`, data) as Promise<number>
}

function deleteReview(rid: number, nid: number): Promise<void> {
  return httpClient.deleteAsync(`/web/c-hdls/api/review/${rid}`)
}

function saveReply(rid: number, data: { owner_reply: string }): Promise<number> {
  return httpClient.putGetJsonAsync<number>(`/web/c-hdls/api/reply/${rid}`, data) as Promise<number>
}

function deleteReply(rid: number): Promise<void> {
  return httpClient.deleteAsync(`/web/c-hdls/api/reply/${rid}`)
}

export default {
  getReviews,
  getComiteeReview,
  saveComiteeReview,
  askForMedal,
  addReview,
  editReview,
  deleteReview,
  saveReply,
  deleteReply,
}
