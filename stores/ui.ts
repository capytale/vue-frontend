import { defineStore } from 'pinia'
import type { TreeExpandedKeys, TreeSelectionKeys } from 'primevue/tree'

export const useSideMenuStore = defineStore('sideMenu', {
  state: () => ({ visible: false }),
  actions: {
    toggle() {
      this.visible = !this.visible
    },
    // toggleHover() {
    //   this.hover = !this.hover;
    // },
  },
})
export const useActiveTagStore = defineStore('activeTag', () => {
  const tids = ref<TreeSelectionKeys>({})
  const expanded = ref<TreeExpandedKeys>({})

  const getTid = () => {
    const tid = Object.keys(tids.value)[0]
    return tid ? safeParseInt(tid) : null
  }

  return { tids, expanded, getTid }
})

function MaverickchangeTheme(currentTheme, newTheme, linkElementId, callback) {
  let _linkElement$parentNo
  const linkElement = document.getElementById(linkElementId)
  const cloneLinkElement = linkElement.cloneNode(true)
  const newThemeUrl = linkElement.getAttribute('href').replace(currentTheme, newTheme)
  cloneLinkElement.setAttribute('id', linkElementId + '-clone')
  cloneLinkElement.setAttribute('href', newThemeUrl)
  cloneLinkElement.addEventListener('load', function () {
    linkElement.remove()
    cloneLinkElement.setAttribute('id', linkElementId)
    if (callback) {
      callback()
    }
  });
  (_linkElement$parentNo = linkElement.parentNode) === null || _linkElement$parentNo === void 0 || _linkElement$parentNo.insertBefore(cloneLinkElement, linkElement.nextSibling)
};

export const useThemeStore = defineStore('theme', {
  state: () => ({ current: 'light' }),
  actions: {
    toggle() {
      const newTheme = this.current === 'light' ? 'dark' : 'light'
      MaverickchangeTheme(this.current, newTheme, 'theme-link')
      this.current = newTheme
    },
  },
})

export const useCodeStore = defineStore('code', {
  state: () => ({ visible: false }),
  actions: {
    toggle() {
      this.visible = !this.visible
    },
  },
})
