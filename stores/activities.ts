import { ApiError } from '@capytale/activity.js/api/http/error'
import {
  apiGetMetadata,
  apiFetchActivities,
  apiPutMetadata,
  apiLoadActivityDetails,
  apiDeleteActivity,
  apiCloneActivity,
  apiLockMode,
  apiMoveActivities,
  apiUntagActivity,
  apiBulkArchive,
  apiReplaceTags,
  apiGetWholeDetails,
} from '~/api/activities'
import type { Activity, ActivityBase, ActivityMetadata } from '~/types/activities'
import type { Status } from '~/types/store'
import { useBibList } from '~/stores/bib'


const _useActivitiesStore = defineStore('activities', () => {
  const bibStore = useBibList(true)

  const activities = ref<Activity[]>([])
  const status = ref<Status>('idle')

  // const tagsToNumber = (actiList: ActivityBase[]) => {
  //   return actiList.map((acti) => {
  //     acti.tags = acti.tags.map((tag) => {
  //       return safeParseInt(tag)
  //     })
  //     acti.nid = safeParseInt(acti.nid)
  //     // acti.changed = safeParseInt(acti.changed)
  //     return acti
  //   })
  // }

  const loadActivities = async () => {
    if (status.value == 'success') return
    if (status.value == 'loading') return
    try {
      status.value = 'loading'
      activities.value = await apiFetchActivities()
      status.value = 'success'
    }
    catch (e) {
      status.value = 'error'
    }
  }

  const getMetadata = async (nid: number | string) => {
    nid = safeParseInt(nid)
    try {
      const metadata = await apiGetMetadata(nid)
      const act = activities.value.find(a => a.nid == nid)
      if (act != null) {
        act.status_shared = metadata?.status_shared
        act.status_web = metadata?.status_web
      }
      return metadata
    }
    catch (e) {
      console.error('Erreur lors de la récupération des métadonnées de l\'activité', e)
    }
  }

  const putMetadata = async (
    nid: number | string,
    data: ActivityMetadata,
  ) => {
    nid = safeParseInt(nid)
    const act = activities.value.find(a => a.nid == nid)
    let currentShared: boolean | undefined
    let currentWeb: boolean | undefined
    if (act != null) {
      // optimistic update
      currentShared = act.status_shared
      currentWeb = act.status_web
      act.status_shared = data.status_shared
      act.status_web = data.status_web
    }
    try {
      await apiPutMetadata(nid, data)
      bibStore.refresh(nid)
    }
    catch (e) {
      // Rollback
      if (act != null) {
        act.status_shared = currentShared!
        act.status_web = currentWeb!
      }
      throw e
    }
  }

  const getActivity = (nid: number | string) => {
    nid = safeParseInt(nid)
    return activities.value.find(a => a.nid == nid)
  }

  const loadActivityDetails = async (nid: number | string) => {
    nid = safeParseInt(nid)
    const activity = getActivity(nid)
    if (activity == null) return
    if (activity.refreshRequested) return
    if (activity.extra) return
    if (activity.detailsRequested) return
    activity.detailsRequested = true

    try {
      const details = await apiLoadActivityDetails(activity.nid)

      if (details == null) return
      delete activity.detailsRequested
      if (activity.refreshRequested) return
      // TODO : solution envisageable si les clés de ActivityDetails sont stables :
      // (Object.keys(details) as Array<keyof ActivityDetails>).forEach((key) => {...
      for (const key in details) activity[key] = details[key]
      activity.extra = true
    }
    catch (e) {
      delete activity.detailsRequested
      throw e
    }
  }

  const loadWholeActivity = async (nid: number | string) => {
    nid = safeParseInt(nid)
    let alreadyLoaded = true
    let activity = getActivity(nid)
    if (activity == null) {
      alreadyLoaded = false
      activity = { nid } as Activity
    }
    if (activity.refreshRequested) return
    activity.refreshRequested = true
    try {
      const details = await apiGetWholeDetails(nid)
      delete activity.refreshRequested
      for (const key in details) activity[key] = details[key]
      activity.extra = true
      if (!alreadyLoaded) {
        activities.value.push(activity)
      }
    }
    catch (e) {
      delete activity.refreshRequested
      console.error('Erreur lors de la récupération des détails de l\'activité', e)
      if (e instanceof ApiError) {
        // TODO : reprendre quand ApiError sera plus précis
        activities.value = activities.value.filter(a => a.nid != nid)
      }
      else
        throw e
    }

    if (!activity.isSa) {
      activities.value.forEach((a) => {
        if ((a.isSa) && (a.activityId == activity.nid)) {
          loadWholeActivity(a.nid)
        }
      })
    }
  }

  const deleteActivity = async (nids: number[]) => {
    console.log('nids', nids)

    try {
      const { deleted, notDeleted } = await apiDeleteActivity(nids)

      for (const nid of deleted) {
        activities.value = activities.value.filter(item => item.nid !== nid)
      }
      return notDeleted
    }
    catch (e) {
      console.error('Erreur lors de la suppression de l\'activité', e)
    }
  }

  // TODO : console.error + throw e font un peu doublon, mais c'est dans l'attente qu'on consolide notre gestion des erreurs

  const cloneActivity = async (nid: number | string, title: string = '') => {
    nid = safeParseInt(nid)
    let r
    try {
      r = await apiCloneActivity(nid, title)
      bibStore.refresh(nid)
    }
    catch (e) {
      console.error('Erreur lors du clonage de l\'activité', e)
      throw e
    }
    loadWholeActivity(r.nid)
    return r
  }

  const lockMode = async (activity: any) => {
    // Optimistic update
    const oldMode = activity.mode
    activity.mode = 'N_X'
    try {
      await apiLockMode(activity.nid, 'lock')
    }
    catch (e) {
      // Rollback
      activity.mode = oldMode
      throw e
    }
  }

  const unlockMode = async (activity: any) => {
    // Optimistic update
    const oldMode = activity.mode
    activity.mode = 'N_O'
    try {
      await apiLockMode(activity.nid, 'unlock')
    }
    catch (e) {
      // Rollback
      activity.mode = oldMode
      throw e
    }
  }

  const moveActivities = async (pxyNids: ActivityBase[], tid: number) => {
    for (const item of pxyNids) {
      activities.value = activities.value.map(el => el.nid == item.nid ? { ...el, tags: [tid] } : el)
    }
    const nids = [...pxyNids.map(o => o.nid)]
    await apiMoveActivities(nids, tid)
  }

  const untagActivity = async (nid: number, tid: number) => {
    const obj = activities.value.find(el => el.nid == nid)
    if (!obj) return
    const arrayTids = obj.tags.filter(item => item !== tid)
    activities.value = activities.value.map(el => el.nid == nid ? { ...el, tags: arrayTids } : el)
    await apiUntagActivity(nid, tid)
  }

  const changeMyVueCount = (nid: number, a: number, b: number, c: number, d: number) => {
    activities.value = activities.value.map(el => el.nid == nid ? { ...el, viewsDetails: { 100: a, 200: b, 300: c, visible: d } } : el)
  }

  const bulkArchive = async (nids: number[], corbeilleTid: number) => {
    for (const nid of nids) {
      activities.value = activities.value.map(el => el.nid == nid ? { ...el, viewsDetails: { 100: 0, 200: 0, 300: 0, visible: 0 } } : el)
      await apiBulkArchive(nid, corbeilleTid)
      // TODO : il faudrait peut-être gérer les erreurs ici
    }
  }

  const replaceTags = async (nids: any[], tids: any[]) => {
    await apiReplaceTags(nids, tids)
  }

  return {
    activities,
    status,
    loadActivities,
    getMetadata,
    putMetadata,
    getActivity,
    loadActivityDetails,
    loadWholeActivity,
    deleteActivity,
    cloneActivity,
    lockMode,
    unlockMode,
    moveActivities,
    untagActivity,
    changeMyVueCount,
    bulkArchive,
    replaceTags,
  }
})

function useActivitiesStore(lazy: boolean = false): ReturnType<typeof _useActivitiesStore> {
  const store = _useActivitiesStore();
  if (!lazy) store.loadActivities();
  return store;
}

export { useActivitiesStore }