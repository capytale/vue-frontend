import httpClient from '@capytale/activity.js/backend/capytale/http'
import evalApi from '@capytale/activity.js/backend/capytale/evaluation'
import type { EvaluationList } from '@capytale/activity.js/activity/evaluation/evaluation'
import type { Assignments } from '~/types/assignments'

// Definit le endpoint de l'API
const assignmentsApiEp = '/web/c-hdls/api/assignments'

export const useAssignmentsStore = defineStore('assignments', () => {
  const activites = useActivitiesStore()

  const assignments = ref<Assignments>()
  const detailedEvaluations = ref<EvaluationList>()
  const loadingAssignments = ref(false)

  const refreshIfSelfEval = (data: Assignments) => {
    if (data.tab == null) return
    data.tab.forEach((assignment) => {
      activites.activities.forEach((activity) => {
        if (activity.nid === assignment.sa_nid) {
          activites.loadWholeActivity(activity.nid)
        }
      })
    })
  }

  /**
   * Transforme les données brutes du serveur pour s'assurer que les données manipulées au sein de l'application sont bien des number, pour les champs concernés.
   *
   * @param data Données brutes renvoyées par le serveur.
   * @returns data Les données traitées, string parsées en number.
   */
  const STNAssignments = (data: Assignments) => {
    data.nid = safeParseInt(data.nid)
    data.tab.forEach((assignment) => {
      assignment.sa_nid = safeParseInt(assignment.sa_nid)
      assignment.sa_uid = safeParseInt(assignment.sa_uid)
      assignment.changed = safeParseInt(assignment.changed)
      assignment.tags = assignment.tags.map(safeParseInt)
    })
    return data
  }

  const getAssignments = async (nid: number) => {
    detailedEvaluations.value = undefined
    loadingAssignments.value = true
    const data = (await httpClient.getJsonAsync<Assignments>('/web/c-hdls/api/assignments/' + nid))!

    assignments.value = STNAssignments(data)

    if (assignments.value.hasDetailedEvaluation) {
      detailedEvaluations.value = await evalApi.listEvals(nid as any) // TODO ça ira mieux bientôt...
    }
    loadingAssignments.value = false
  }

  const saveAppr = async (nid: string, appr: string) => {
    console.log('saveAppr', nid, appr)
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'saveAppr', nid, appr: appr },
    )
    refreshIfSelfEval(assignments.value!)
  }

  const saveEval = async (nid: string, evalu: string) => {
    console.log("saveEval", nid, evalu)
    assignments.value.tab.find(o => nid == o.sa_nid).evaluation = evalu

    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'saveEval', nid, evalu: evalu },
    )
    refreshIfSelfEval(assignments.value!)
  }

  const changeSaWf = async (sa_nid: number | number[], newWorkflow: '100' | '200' | '300') => {
    if (!Array.isArray(sa_nid)) {
      sa_nid = [sa_nid]
    }
    assignments.value!.tab.forEach((assignment) => {
      if (sa_nid.includes(assignment.sa_nid)) {
        assignment.workflow = newWorkflow
      }
    })
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'saveSaWf', nids: sa_nid, newWorkflow },
    )
  }
  const unHide = async (sa_nid: number, corbeilleTid: number) => {
    if (assignments.value == null) return

    assignments.value.tab = assignments.value.tab.map(el => el.sa_nid == sa_nid ? { ...el, tags: [] } : el)
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'unHide', sa_nid, corbeilleTid },
    )
  }
  const hide = async (sa_nid: number, corbeilleTid: number) => {
    if (assignments.value == null) return

    assignments.value.tab = assignments.value.tab.map(el => el.sa_nid == sa_nid ? { ...el, tags: [corbeilleTid] } : el)
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'hide', sa_nid, corbeilleTid },
    )
  }
  const archive = async (sa_nids: number[], corbeilleTid: number) => {
    // console.log("archive", sa_nids, corbeilleTid)
    if (assignments.value == null) return

    for (const sa_nid of sa_nids) {
      assignments.value.tab = assignments.value.tab.map(el => el.sa_nid == sa_nid ? { ...el, tags: [corbeilleTid] } : el)
    }
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'archive', sa_nids, corbeilleTid },
    )
  }
  const unArchive = async (sa_nids: number[], corbeilleTid: number) => {
    if (assignments.value == null) return

    for (const sa_nid of sa_nids) {
      assignments.value.tab = assignments.value.tab.map(el => el.sa_nid == sa_nid ? { ...el, tags: [] } : el)
    }
    await httpClient.postJsonAsync(
      assignmentsApiEp,
      { action: 'unArchive', sa_nids, corbeilleTid },
    )
  }
  return {
    assignments,
    detailedEvaluations,
    loadingAssignments,
    getAssignments,
    saveAppr,
    saveEval,
    changeSaWf,
    unHide,
    hide,
    archive,
    unArchive,
  }
},
)
