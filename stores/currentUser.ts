import { loadCurrentUser, type CurrentUser } from '~/utils/currentUser'
import type { Status } from '~/types/store'

export const useCurrentUserStore = defineStore('user', () => {
  const user = ref<CurrentUser | null>()
  const status = shallowRef<Status>('idle')

  const loadUser = async () => {
    if (status.value == 'success') return
    if (status.value == 'loading') return
    try {
      status.value = 'loading'
      user.value = await loadCurrentUser()
      status.value = 'success'
      if (!user.value.firstname) {
        user.value.firstname = '_Sans prénom'
      }
      if (!user.value.lastname) {
        user.value.lastname = '_Sans nom'
      }
    }
    catch (e) {
      status.value = 'error'
    }
  }
  loadUser()

  const isAuthenticated = computed(() => (!!user.value))
  const isTeacher = computed(() => (user.value && (user.value.profil === 'teacher')))
  const isStudent = computed(() => (user.value && (user.value.profil === 'student')))
  const isAdmin = computed(() => (user.value && user.value.roles.includes('administrator')))

  return {
    user,
    status,
    isAuthenticated,
    isTeacher,
    isStudent,
    isAdmin,
  }
})
