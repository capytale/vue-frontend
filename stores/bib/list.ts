import { ApiError } from '@capytale/activity.js/api/http/error'
import bibApi from "~/api/bib";

import type { BibActivityDetail as BaseBibActivityDetail } from "~/types/bib";
import type { Status } from '~/types/store'

interface BibActivityDetail extends BaseBibActivityDetail {
  auteur: string;
  fullAbstract?: string;
}

export type { BibActivityDetail }

interface InternalBibActivityDetail extends BibActivityDetail {
  fullAbstractStatus?: Status;
}

function buildAuthor(a: BibActivityDetail): string {
  let nom = a.nom;
  if (nom == null || nom == '') nom = null;
  else nom.trim();
  let prenom = a.prenom;
  if (prenom == null || prenom == '') prenom = null;
  else prenom.trim();
  const r = '';
  if (nom == null) {
    return '';
  }
  else {
    if (prenom == null) return nom;
    else return prenom + ' ' + nom;
  }
}

const _useBibList = defineStore('bibliotheque', () => {
  const list = ref<BibActivityDetail[]>([]);
  const status = ref<Status>('idle');


  /**
   * Charge les activités de la bibliothèque.
   *
   * @param forceReload force le rechargement
   */
  async function load(forceReload: boolean = false): Promise<void> {
    if (status.value == 'loading') return;
    if (forceReload || (status.value !== 'success')) {
      status.value = 'loading';
      try {
        const l = (await bibApi.fetchActivities()) as BibActivityDetail[];
        for (let i = 0; i < l.length; ++i) {
          const a = l[i];
          a.auteur = buildAuthor(a);
        }
        list.value = l
        status.value = 'success';

      } catch (e) {
        status.value = 'error';
      }
    }
  }

  function remove(nid: number): void {
    const l = list.value;
    const i = l.findIndex(a => a.nid === nid);
    if (i >= 0) {
      l.splice(i, 1);
    }
  }

  /**
   * Recharge une activité. Si elle n'existe pas, la retire de la liste.
   * Ne fait rien si le store n'est pas chargé.
   * La date n'est pas mise à jour.
   * 
   * @param nid 
   */
  async function refresh(nid: number): Promise<void> {
    if (status.value !== 'success') return;
    try {
      const data = (await bibApi.fetchOneActivity(nid)) as BibActivityDetail
      if (data == null) {
        remove(nid);
      } else {
        data.auteur = buildAuthor(data);
        const l = list.value;
        const i = l.findIndex(a => a.nid === nid);
        if (i >= 0) {
          data.changed = l[i].changed;
          l[i] = data;
        }
        else {
          l.push(data);
        }
      }
    }
    catch (error) {
      if (!(error instanceof ApiError)) throw error;
      // Une erreur ici signifie que l'activité n'existe pas ou n'est plus dans la bibliothèque
      // On la retire de la liste
      remove(nid);
    }
  }

  async function loadFullAbstract(nid: number): Promise<void> {
    const l = list.value;
    const i = l.findIndex(a => a.nid === nid);
    if (i < 0) return;
    const a = l[i] as InternalBibActivityDetail;
    if (a.fullAbstractStatus === 'loading') return;
    try {
      let fa = await bibApi.fetchFullAbstract(nid);
      a.fullAbstract = fa;
      a.fullAbstractStatus = 'success';
    }
    catch (e) {
      a.fullAbstractStatus = 'error';
    }
  }

  return {
    list,
    status,
    load,
    loadFullAbstract,
    refresh,
  }
})



/**
 * Fournit un objet réactif.
 *
 * @param lazy retarde le chargement depuis le backend
 * @returns
 */
function useBibList(lazy: boolean = false): ReturnType<typeof _useBibList> {
  const store = _useBibList();
  if (!lazy) store.load();
  return store;
}

export { useBibList }
