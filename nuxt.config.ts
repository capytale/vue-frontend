import Lara from '@primevue/themes/lara'
import { definePreset } from '@primevue/themes'
// import 'dotenv/config';

const capyPreset = definePreset(Lara, {
  primitive: {
    capycolor: { 50: '#f3f7fc', 100: '#e5eef9', 200: '#c6dcf1', 300: '#93bfe6', 400: '#5a9ed6', 500: '#327bb8', 600: '#2567a4', 700: '#1f5285', 800: '#1d466f', 900: '#1d3c5d', 950: '#14273d' },
  },
  semantic: {
    primary: {
      50: '{emerald.50}',
      100: '{capycolor.100}',
      200: '{capycolor.200}',
      300: '{capycolor.300}',
      400: '{capycolor.400}',
      500: '{capycolor.500}',
      600: '{capycolor.600}',
      700: '{capycolor.700}',
      800: '{capycolor.800}',
      900: '{capycolor.900}',
      950: '{capycolor.950}',
    },
  },
  components: {
    card: {
      colorScheme: {
        light: {
          shadow: '0 1px 3px 0 var(--p-surface-300), 0 1px 2px -1px var(--p-surface-300)',
        },
        dark: {
          shadow: '0 1px 3px 0 var(--p-surface-800), 0 1px 2px -1px var(--p-surface-800)',
        },
      },
    },
  },
})

let config: Parameters<typeof defineNuxtConfig>[0] = {
  modules: ['@primevue/nuxt-module', '@nuxtjs/color-mode', '@pinia/nuxt', '@vueuse/nuxt', '@nuxtjs/tailwindcss', '@vesp/nuxt-fontawesome', '@nuxt/eslint'],
  ssr: false,

  devtools: { enabled: false },

  app: {
    baseURL: '/',
    // head: {
    //   link: [
    //     {
    //       id: 'theme-link',
    //       rel: 'stylesheet',
    //       href: baseUrl + 'themes/lara-light-blue/theme.css'
    //       //href: '/' + 'themes/lara-light-blue/theme.css'
    //     }
    //   ]
    // }
  },
  css: [
    'primeicons/primeicons.css',
    'assets/css/tailwind.css',
  ],

  colorMode: {
    classSuffix: '',
  },

  compatibilityDate: '2024-08-26',

  eslint: {
    config: {
      stylistic: true,
    },
  },

  fontawesome: {
    icons: {
      brands: ['creative-commons'],
      solid: [
        'medal', 'award', 'ban', 'moon', 'sun', 'archive',
        'balance-scale', 'glasses', 'lightbulb', 'recycle', 'bullseye', 'spell-check',
        'times', 'minus', 'check', 'heart',
        'frown', 'meh-blank', 'smile', 'grin-stars',
        'thumbs-up', 'thumbs-down',
        'sign-in-alt', 'table', "comment-dots"
      ],
      regular: ['thumbs-up', 'thumbs-down'],
    },
  },

  primevue: {
    usePrimeVue: true,
    options: {
      theme: {
        cssLayer: {
          name: 'primevue',
          order: 'tailwind-base, primevue, tailwind-utilities',
        },
        preset: capyPreset,
        options: {
          darkModeSelector: '.dark',
        },
      },

    },
  },
};

if (process.env.NODE_ENV === 'development') {
  config.devtools = { enabled: true };
}

if (
  process.env.CAPDEV_BACKEND_IP &&
  process.env.CAPDEV_SSL_KEY &&
  process.env.CAPDEV_SSL_CERT
) {
  config.devServer = {
    https: {
      key: process.env.CAPDEV_SSL_KEY,
      cert: process.env.CAPDEV_SSL_CERT,
    }
  };

  const ip = process.env.CAPDEV_BACKEND_IP;
  config.nitro = {
    devProxy: {
      '/vanilla': { target: `https://${ip}/vanilla`, changeOrigin: false, secure: false },
      // '/p': { target: `https://capytale2.ac-paris.fr/p`, changeOrigin: true, secure: false },
      '/p': { target: `https://capytaledev.ac-paris.fr/p`, changeOrigin: true, secure: false },
      '/web': { target: `https://${ip}/web`, changeOrigin: false, secure: false },
      '/logo.svg': { target: `https://${ip}/logo.svg`, changeOrigin: false, secure: false, ignorePath: true },
      '/logo-rect.svg': { target: `https://${ip}/logo-rect.svg`, changeOrigin: false, secure: false, ignorePath: true },
    },
    routeRules: {
      '/~/**': {
        redirect: '/**'
      }
    }
  };
}

export default defineNuxtConfig(config);
