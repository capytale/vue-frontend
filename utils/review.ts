const critLogos = {
  fon: { fa: 'lightbulb', title: 'Fond', tooltip: 'Fond : Un vrai travail de fond a été réalisé sur l’activité, y compris sur un thème simple. L’activité permet aux autres utilisateurs de gagner du temps, ou elle apporte des idées intéressantes pour traiter un thème.' },
  uti: { fa: 'recycle', title: 'Utilisabilité', tooltip: 'Utilisabilité : L’activité est utilisable en l’état ou presque.' },
  per: { fa: 'bullseye', title: 'Pertinence', tooltip: 'Pertinence : Il y a adéquation entre le titre, les objectifs annoncés dans le descriptif de l’activité et le contenu de l’activité.' },
  app: { fa: 'spell-check', title: 'Apparence', tooltip: 'Apparence : L’orthographe, la grammaire et la typographie sont de bonne qualité.' },
  clr: { fa: 'glasses', title: 'Clarté', tooltip: 'Clarté : Les consignes pour le public cible (élève, enseignant) sont claires.' },
}

const thumbList = {
  down: { fa: 'fa-solid fa-thumbs-down', value: -1, tt: "Insuffisant", class: 'text-red-500 text-xl' },
  idle: { fa: "fa-regular fa-thumbs-up", value: 0, tt: "Sans opinion ou passable", class: 'text-gray-300 text-xl' },
  up: { fa: 'fa-solid fa-thumbs-up', value: 1, tt: "Bien", class: 'text-green-500 text-xl' },
}

const thumb = (x) => {
  if (x < 0) return thumbList.down
  if (x > 0) return thumbList.up
  return thumbList.idle
}
export { critLogos, thumbList, thumb }
