export default (s: string | number) => {
  return typeof s === 'string' ? parseInt(s) : s
}
