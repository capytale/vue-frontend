import type { Store } from 'pinia'
import type { ToastServiceMethods } from 'primevue/toastservice'
import type { Activity } from '~/types/activities'
import type { Tag } from '~/types/tags'

const nbSelected = (selectedNids: string[]) => {
  if (selectedNids.length == 1) return '1 élément sélectionné '
  return selectedNids.length + ' éléments sélectionnés '
}

const iAm = [
  { label: 'Créateur', code: 'cr', img: '/web/modules/custom/capytale_ui/assets/owner-color.svg' },
  { label: 'Apprenant', code: 'ap', img: '/web/modules/custom/capytale_ui/assets/student-color.svg' },
  { label: 'Associé', code: 'as', img: '/web/modules/custom/capytale_ui/assets/associate-color.svg' },
]

const getIAmLabel = (code: string) => {
  return iAm.find(o => o.code == code)!.label
}
const getIAmImg = (code: string) => {
  return iAm.find(o => o.code == code)!.img
}

const corbeilleTid = (tags: Tag[]) => {
  const corbeilleTag = tags.find(o => o.label === 'Corbeille')
  if (!corbeilleTag) return null
  return corbeilleTag.id
}

export { nbSelected, iAm, getIAmLabel, getIAmImg, corbeilleTid }
