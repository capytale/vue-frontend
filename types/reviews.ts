export interface Review {
  uid: number
  content: string
  criteria: { [key: string]: number }
  prenom: string
  nom: string
}

export interface ComiteeReview {
  message: string
  discuss: string
  status: "medal" | "accepted" | "trash" | "debate"
  ask: boolean
}

export interface FullReview {
  rid: number
  nid: number
  uid: number
  content: string
  criteria: { [key: string]: number }
  report: string
  created: number
  changed: number
  owner_reply: string
  reply_created: number
  reply_changed: number
}
