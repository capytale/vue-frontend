export interface Assignment {
  changed: number
  sa_nid: number
  sa_uid: number
  nom: string
  prenom: string
  classe: string | null
  etab: string | null
  status: 0 | 1
  workflow: 100 | 200 | 300
  appreciation: string
  evaluation: string
  player: string
  tags: number[]
}

export interface Assignments {
  icon: string
  status: string
  status_clonable: string
  access_tr_mode: string
  type: string
  is_in_time_range: boolean
  dt_deb: number
  dt_end: number
  title: string
  nid: number
  tab: Assignment[]
  hasDetailedEvaluation: boolean
}

// TODO : les nid et uid à passer en number, comme pour les activities ?
