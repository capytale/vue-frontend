
export interface BibActivityDetail {
  nid: number;
  type: string;
  title: string;
  changed: number;
  prenom: string | null;
  nom: string | null;
  abstract: string | null;
  abstract_truncated: boolean;
  nb_clone: number;
  enseignement: string[];
  niveau: string[];
  for_all: boolean;
}