export type ActivityBase = {
  nid: number
  type: string
  title: string
  changed: string
  isSa: boolean
  whoami: string
  tags: number[]
}

export type ActivityMetadata = {
  status_shared: boolean
  status_web: boolean
  abstract: string
  enseignements: string[]
  niveaux: string[]
  modules: number[]
  themes: number[]
}

export type ActivityDetails = {
  status_clonable: boolean
  access_tr_mode: string
  tr_beg: string
  tr_end: string
  mode: string
  whoami: string
  boss: string
  status_shared: boolean
  status_web: boolean
  isSa: boolean
  code: string
  changed: string
  viewsTotal: string
  viewsDetails: {
    100: number
    200: number
    300: number
    visible: number
  }
  [key: string]: any // Ajout de l'index signature
}

export type Activity = ActivityBase & Partial<ActivityDetails>
