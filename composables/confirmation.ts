/**
 * Un wrapper pour la boîte de dialogue de confirmation de primevue
 */
import { useConfirm } from "primevue/useconfirm";

type ConfirmOptions = {
  header: string;
  message: string;
  acceptLabel?: string;
  rejectLabel?: string;
  severity?: 'info' | 'warn' | 'danger';
}

type FullConfirmOptions =  Required<ConfirmOptions>;

const defaultOptions: FullConfirmOptions = {
  header: 'Confirmation',
  message: 'Êtes-vous sûr ?',
  acceptLabel: 'Valider',
  rejectLabel: 'Annuler',
  severity: 'info'
}

const severityMap = {
  info: {
    icon: 'pi pi-info-circle',
    acceptClass: 'p-button-primary'
  },
  warn: {
    icon: 'pi pi-exclamation-triangle',
    acceptClass: 'p-button-warn'
  },
  danger: {
    icon: 'pi pi-times-circle',
    acceptClass: 'p-button-danger'
  }
}

export function useConfirmWrapper() {
  const confirm = useConfirm();

  return function (options: ConfirmOptions): Promise<boolean> {
    options = { ...defaultOptions, ...options } as FullConfirmOptions;
    let map: (typeof severityMap)[FullConfirmOptions['severity']] = severityMap[options.severity!];
    if (map == null) map = severityMap.info;

    return new Promise<boolean>((resolve) => {
      confirm.require({
        header: options.header,
        message: options.message,
        rejectLabel: options.rejectLabel,
        acceptLabel: options.acceptLabel,
        icon: map.icon,
        acceptClass: map.acceptClass,
        rejectClass: 'p-button-secondary p-button-outlined',

        accept() { resolve(true); },
        reject() { resolve(false); }
      });
    });
  }
}

